// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FMODSTUDIO_FMODEvent_generated_h
#error "FMODEvent.generated.h already included, missing '#pragma once' in FMODEvent.h"
#endif
#define FMODSTUDIO_FMODEvent_generated_h

#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_SPARSE_DATA
#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_RPC_WRAPPERS
#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFMODEvent(); \
	friend struct Z_Construct_UClass_UFMODEvent_Statics; \
public: \
	DECLARE_CLASS(UFMODEvent, UFMODAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FMODStudio"), NO_API) \
	DECLARE_SERIALIZER(UFMODEvent)


#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUFMODEvent(); \
	friend struct Z_Construct_UClass_UFMODEvent_Statics; \
public: \
	DECLARE_CLASS(UFMODEvent, UFMODAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FMODStudio"), NO_API) \
	DECLARE_SERIALIZER(UFMODEvent)


#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFMODEvent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFMODEvent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFMODEvent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFMODEvent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFMODEvent(UFMODEvent&&); \
	NO_API UFMODEvent(const UFMODEvent&); \
public:


#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFMODEvent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFMODEvent(UFMODEvent&&); \
	NO_API UFMODEvent(const UFMODEvent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFMODEvent); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFMODEvent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFMODEvent)


#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_PRIVATE_PROPERTY_OFFSET
#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_13_PROLOG
#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_PRIVATE_PROPERTY_OFFSET \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_SPARSE_DATA \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_RPC_WRAPPERS \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_INCLASS \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_PRIVATE_PROPERTY_OFFSET \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_SPARSE_DATA \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_INCLASS_NO_PURE_DECLS \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class FMODEvent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FMODSTUDIO_API UClass* StaticClass<class UFMODEvent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODEvent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
