// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FMODSTUDIO_FMODAmbientSound_generated_h
#error "FMODAmbientSound.generated.h already included, missing '#pragma once' in FMODAmbientSound.h"
#endif
#define FMODSTUDIO_FMODAmbientSound_generated_h

#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_SPARSE_DATA
#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_RPC_WRAPPERS
#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFMODAmbientSound(); \
	friend struct Z_Construct_UClass_AFMODAmbientSound_Statics; \
public: \
	DECLARE_CLASS(AFMODAmbientSound, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FMODStudio"), NO_API) \
	DECLARE_SERIALIZER(AFMODAmbientSound)


#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAFMODAmbientSound(); \
	friend struct Z_Construct_UClass_AFMODAmbientSound_Statics; \
public: \
	DECLARE_CLASS(AFMODAmbientSound, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/FMODStudio"), NO_API) \
	DECLARE_SERIALIZER(AFMODAmbientSound)


#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFMODAmbientSound(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFMODAmbientSound) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFMODAmbientSound); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFMODAmbientSound); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFMODAmbientSound(AFMODAmbientSound&&); \
	NO_API AFMODAmbientSound(const AFMODAmbientSound&); \
public:


#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFMODAmbientSound(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFMODAmbientSound(AFMODAmbientSound&&); \
	NO_API AFMODAmbientSound(const AFMODAmbientSound&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFMODAmbientSound); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFMODAmbientSound); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFMODAmbientSound)


#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_PRIVATE_PROPERTY_OFFSET
#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_10_PROLOG
#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_PRIVATE_PROPERTY_OFFSET \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_SPARSE_DATA \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_RPC_WRAPPERS \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_INCLASS \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_PRIVATE_PROPERTY_OFFSET \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_SPARSE_DATA \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_INCLASS_NO_PURE_DECLS \
	NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h_13_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class FMODAmbientSound."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FMODSTUDIO_API UClass* StaticClass<class AFMODAmbientSound>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID NotimPortant_Plugins_FMODStudio_Source_FMODStudio_Classes_FMODAmbientSound_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
